package ru.aushakov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.enumerated.Role;

@Setter
@Getter
public final class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String lastName;

    @Nullable
    private String firstName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USER;

    private boolean lockedFlag = false;

    public User(@NotNull String login, @NotNull String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

}
