package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getTerminalCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

}
