package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    void add(E entity);

    void remove(E entity);

    void clear();

    E findOneById(String id);

    E findOneByIndex(Integer index);

    E removeOneById(String id);

    E removeOneByIndex(Integer index);

}
