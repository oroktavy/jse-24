package ru.aushakov.tm.repository;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IRepository;
import ru.aushakov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Override
    @NotNull
    public List<E> findAll() {
        return list;
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return list.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void add(@NotNull final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    @Nullable
    public E findOneById(@NotNull final String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public E findOneByIndex(@NotNull final Integer index) {
        return list.get(index);
    }

    @Override
    @Nullable
    public E removeOneById(@NotNull final String id) {
        @Nullable final E entity = findOneById(id);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

    @Override
    @Nullable
    public E removeOneByIndex(@NotNull final Integer index) {
        @Nullable final E entity = findOneByIndex(index);
        Optional.ofNullable(entity).ifPresent(list::remove);
        return entity;
    }

}
