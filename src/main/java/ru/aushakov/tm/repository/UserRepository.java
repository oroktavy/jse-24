package ru.aushakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.repository.IUserRepository;
import ru.aushakov.tm.model.User;
import ru.aushakov.tm.util.HashUtil;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findOneByLogin(@NotNull final String login) {
        return list.stream()
                .filter(u -> login.equalsIgnoreCase(u.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User removeOneByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(list::remove);
        return user;
    }

    @Override
    @Nullable
    public User setPassword(
            @NotNull final String login,
            @NotNull final String newPassword
    ) {
        @Nullable final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(u -> u.setPasswordHash(HashUtil.salt(newPassword)));
        return user;
    }

    @Override
    @Nullable
    public User updateOneById(
            @NotNull final String id,
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName
    ) {
        @Nullable final User user = findOneById(id);
        Optional.ofNullable(user).ifPresent(u -> {
            u.setLastName(lastName);
            u.setFirstName(firstName);
            u.setMiddleName(middleName);
        });
        return user;
    }

    @Override
    @Nullable
    public User updateOneByLogin(
            @NotNull final String login,
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName
    ) {
        @Nullable final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(u -> {
            u.setLastName(lastName);
            u.setFirstName(firstName);
            u.setMiddleName(middleName);
        });
        return user;
    }

    @Override
    @Nullable
    public User lockOneByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(u -> u.setLockedFlag(true));
        return user;
    }

    @Override
    @Nullable
    public User unlockOneByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        Optional.ofNullable(user).ifPresent(u -> u.setLockedFlag(false));
        return user;
    }

}
