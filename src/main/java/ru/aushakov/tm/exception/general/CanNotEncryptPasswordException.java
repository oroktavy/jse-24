package ru.aushakov.tm.exception.general;

import org.jetbrains.annotations.NotNull;

public class CanNotEncryptPasswordException extends RuntimeException {

    public CanNotEncryptPasswordException() {
        super("Can not encrypt password using MD5! Input value may be empty!");
    }

    public CanNotEncryptPasswordException(@NotNull Throwable cause) {
        super("Can not encrypt password using MD5!", cause);
    }

}
