package ru.aushakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.ArgumentConst;
import ru.aushakov.tm.constant.TerminalConst;

public final class VersionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_VERSION;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ArgumentConst.ARG_VERSION;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
